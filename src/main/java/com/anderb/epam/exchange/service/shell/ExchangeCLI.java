package com.anderb.epam.exchange.service.shell;

import com.anderb.epam.exchange.model.Bid;
import com.anderb.epam.exchange.model.BidType;
import com.anderb.epam.exchange.model.Trade;
import com.anderb.epam.exchange.service.exchange.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.stream.Collectors;

/**
 * A class for various shell interactions.
 */
@ShellComponent
public class ExchangeCLI {

    private TradeService tradeService;

    @Autowired
    public ExchangeCLI(TradeService tradeService) {
        this.tradeService = tradeService;
    }

    @ShellMethod("Buy")
    public String buy(double price,
                      long quantity) {

        Bid bid = new Bid(BidType.BUY, price, quantity);
        Trade buyTrade = tradeService.sell(bid);
        return "OK";
    }

    @ShellMethod("Sell")
    public String sell(double price,
                       long quantity) {

        Bid bid = new Bid(BidType.SELL, price, quantity);
        Trade sellTrade = tradeService.sell(bid);
        return "OK";
    }

    @ShellMethod("List of trades")
    public String list() {

        return tradeService.getTrades().stream()
                .map(Object::toString)
                .collect(Collectors.joining("\n"));

    }

    @ShellMethod("List of bids")
    public String bids() {

        return tradeService.getBids().stream()
                .map(Object::toString)
                .collect(Collectors.joining("\n"));

    }


}
