package com.anderb.epam.exchange.service.exchange;

import com.anderb.epam.exchange.model.Bid;
import com.anderb.epam.exchange.model.Trade;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TradeServiceImpl implements TradeService {

    private List<Bid> toBuyBids = new ArrayList<>();
    private List<Bid> toSellBids = new ArrayList<>();
    private List<Trade> trades = new ArrayList<>();

    @Override
    public Trade sell(Bid bid) {
        final Trade trade = new Trade();
        trade.setQuantityAlreadyTraded(0L);
        trade.setBidOrder(bid);
        toBuyBids.stream()
                .filter((buyingBid) -> buyingBid.getQuantity() > 0 && trade.getBidOrder().getPrice() <= buyingBid.getPrice())
                .forEach(sellBid -> {
                    Long quantityWeCanSell = sellBid.getQuantity();
                    Long quantityWeNeedSell = trade.getBidOrder().getQuantity() - trade.getQuantityAlreadyTraded();
                    if (quantityWeNeedSell > 0 && quantityWeCanSell > quantityWeNeedSell) {
                       sellBid.setQuantity(sellBid.getQuantity() - quantityWeNeedSell);
                       trade.setQuantityAlreadyTraded(trade.getBidOrder().getQuantity());
                    } else if (quantityWeNeedSell > 0 && quantityWeCanSell <= quantityWeNeedSell) {
                        sellBid.setQuantity(0L);
                        trade.setQuantityAlreadyTraded(trade.getQuantityAlreadyTraded() + quantityWeCanSell);
                    }

                });
        toSellBids.add(bid);
        trades.add(trade);
    return trade;
    }

    @Override
    public Trade buy(Bid bid) {
        final Trade trade = new Trade();
        trade.setQuantityAlreadyTraded(0L);
        trade.setBidOrder(bid);
        toSellBids.stream()
                .filter(sellingBid -> sellingBid.getQuantity() > 0 && trade.getBidOrder().getPrice() <= sellingBid.getPrice())
                .forEach(sellBid -> {
                    Long quantityWeCanBuy = sellBid.getQuantity();
                    Long quantityNeeds = trade.getBidOrder().getQuantity() - trade.getQuantityAlreadyTraded();
                    if (quantityNeeds > 0 && quantityWeCanBuy > quantityNeeds) {
                       sellBid.setQuantity(sellBid.getQuantity() - quantityNeeds);
                       trade.setQuantityAlreadyTraded(trade.getBidOrder().getQuantity());
                    } else if (quantityNeeds > 0 && quantityWeCanBuy <= quantityNeeds) {
                        sellBid.setQuantity(0L);
                        trade.setQuantityAlreadyTraded(trade.getQuantityAlreadyTraded() + quantityWeCanBuy);
                    }

                });
        toBuyBids.add(bid);
        trades.add(trade);
        return trade;
    }

    @Override
    public List<Trade> getTrades() {
        return trades;
    }

    @Override
    public List<Bid> getBids() {
        ArrayList<Bid> bids = new ArrayList<>(toBuyBids);
        bids.addAll(toSellBids);
        return bids;
    }
}
