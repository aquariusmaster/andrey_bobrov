package com.anderb.epam.exchange.service.exchange;

import com.anderb.epam.exchange.model.Bid;
import com.anderb.epam.exchange.model.Trade;

import java.util.List;

public interface TradeService {
    Trade sell(Bid bid);
    Trade buy(Bid bid);
    List<Trade> getTrades();

    List<Bid> getBids();
}
