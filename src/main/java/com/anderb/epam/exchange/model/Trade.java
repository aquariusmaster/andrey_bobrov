package com.anderb.epam.exchange.model;

public class Trade {

    private Bid bidOrder;
    private Long quantityAlreadyTraded;

    public Bid getBidOrder() {
        return bidOrder;
    }

    public void setBidOrder(Bid bidOrder) {
        this.bidOrder = bidOrder;
    }

    public Long getQuantityAlreadyTraded() {
        return quantityAlreadyTraded;
    }

    public void setQuantityAlreadyTraded(Long quantityAlreadyTraded) {
        this.quantityAlreadyTraded = quantityAlreadyTraded;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "bidOrder=" + bidOrder +
                ", quantityAlreadyTraded=" + quantityAlreadyTraded +
                '}';
    }
}
