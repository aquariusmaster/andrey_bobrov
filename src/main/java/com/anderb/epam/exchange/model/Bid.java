package com.anderb.epam.exchange.model;

import java.util.Objects;

public class Bid {

    private BidType type;
    private Double price;
    private Long quantity;

    public Bid() {
    }

    public Bid(BidType type, Double price, Long quantity) {
        this.type = type;
        this.price = price;
        this.quantity = quantity;
    }

    public BidType getType() {
        return type;
    }

    public void setType(BidType type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bid bid = (Bid) o;
        return type == bid.type &&
                Objects.equals(price, bid.price) &&
                Objects.equals(quantity, bid.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, price, quantity);
    }

    @Override
    public String toString() {
        return "Bid{" +
                "type=" + type +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
